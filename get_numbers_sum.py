import argparse


def get_argument():
    parser = argparse.ArgumentParser(description='Sum of integers divided by 3 or 7')
    parser.add_argument('integer', metavar='N', type=int, help='a length of range')
    arg = parser.parse_args()
    result = arg.integer
    if not 1 <= result <= 1000000:
        raise ValueError("Input has to be in range (1, 1000000).")
    else:
        return result


def get_numbers_sum(length):
    result = 0
    for n in range(1, length + 1):
        if n % 3 == 0 or n % 7 == 0:
            result += n
    print(result)


if __name__ == '__main__':
    input_value = get_argument()
    get_numbers_sum(input_value)
