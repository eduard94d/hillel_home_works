import requests
from math import sqrt


def extract_data(path):
    r = requests.get(path)
    return r.text


def transform_data(data):
    data_list = data.split('\n')
    student_list = list()
    for row in data_list[1:-1]:
        student = dict()
        new_row = row.split(',')

        student['id'] = int(new_row[0])
        student['height'] = float(new_row[1])
        student['weight'] = float(new_row[2])

        student_list.append(student)

    return student_list


def calculate_mean(data):
    height = 0
    weight = 0
    for row in data:
        height += row.get('height')
        weight += row.get('weight')

    mean_height = height / len(data)
    mean_weight = weight / len(data)

    return mean_height, mean_weight


def calculate_std(data, h_mean, w_mean):
    height_deviation = 0
    weight_deviation = 0

    for row in data:
        height_deviation += (row.get('height') - h_mean) ** 2
        weight_deviation += (row.get('weight') - w_mean) ** 2

    height_std = sqrt(height_deviation / len(data))
    weight_std = sqrt(weight_deviation / len(data))

    print(f"Height mean of the students is {round(h_mean, 2)} with standard deviation {round(height_std, 2)} inches.\n"
          f"Weight mean of the students is {round(w_mean, 2)} with standard deviation {round(weight_std, 2)} pounds.")


def get_students_mean_data():
    path_csv = 'https://raw.githubusercontent.com/varelaz/python-course/master/03-git/hw.csv'
    response = extract_data(path_csv)
    cleaned_data = transform_data(response)
    h, w = calculate_mean(cleaned_data)

    return h, w
