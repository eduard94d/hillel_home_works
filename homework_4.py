import string
import random
import os

from flask import Flask, make_response, request

from homework_3 import get_students_mean_data


app = Flask(__name__)


@app.route('/requirements')
def get_requirements():
    filename = os.path.join(
        os.path.dirname(__file__),
        'requirements.txt'
    )
    with open(filename, 'r', encoding='utf-8') as f:
        requirements = f.read()

        response = make_response(requirements)
        response.headers['Content-Type'] = 'text/plain'

        return response


@app.route('/random')
def get_random():
    try:
        length = int(request.args.get('length', '10'))
    except ValueError:
        return "Length is not a number.", 400
    if not 1 <= length <= 100:
        return "Length has to be in range (1, 100)", 400
    try:
        digits = int(request.args.get('digits', '0'))
    except ValueError:
        return "Digits is not a number."

    if digits not in [0, 1]:
        return "Digits have to be in range (0, 1)."
    else:
        digits_bool = bool(digits)

    digits_chars = string.digits if digits_bool else ''

    chars = string.ascii_lowercase + string.ascii_uppercase + digits_chars
    result_list = []
    for char in range(length):
        result_list.append(random.choice(chars))
    result = ''.join(result_list)

    return result


@app.route('/mean_data')
def get_mean_data():
    height_mean, weight_mean = get_students_mean_data()
    height_mean = round(height_mean, 2)
    weight_mean = round(weight_mean, 2)

    return f"Student's mean height is {height_mean}; mean weight is {weight_mean}.\n"


if __name__ == '__main__':
    app.run()
